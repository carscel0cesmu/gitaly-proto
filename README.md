# gitaly-proto has been deprecated

As of Gitaly 1.58.0 and [gitaly-proto 1.39.0](https://gitlab.com/gitlab-org/gitaly-proto/tree/v1.39.0), changes to the Gitaly protocol must be made in https://gitlab.com/gitlab-org/gitaly/tree/master/proto.
